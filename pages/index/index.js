//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    userInfo: {},   // 用户信息
    hasUserInfo: false,  // 用户授权
    show_view :false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),   //  检测小程序版本兼容
    token: '',
    demoKoulin: '',

    animationData: {},
    wish_index:0,
    wishes_images:[],
    big_wishes_image: app.globalData.imgBaseUrl + '1.png',
    xcxewm:'',
    tct_id: '',
    wishImg: ''
  },
  onLoad:function(){
    console.log(app.globalData.imgBaseUrl,this.data.big_wishes_image)
    var self = this;
    if (app.globalData.userInfo && app.globalData.token) {
      var info = app.globalData.userInfo,
        tok = app.globalData.token,
        randomNum = self.randomNum(0, app.globalData.demoKoulin.length - 1),
        demoremark = app.globalData.demoKoulin[randomNum];
      self.setData({
        userInfo: info,
        hasUserInfo: true,
        token: tok,
        show_view:true,
        demoKoulin:demoremark
      })
      
      self.randomWishes()
    } else {
      self.loop()
    }
    var that = this;
    if (that.data.xcxewm === '') {
      //二维码
      var postUrl = app.setConfig.url + '/index.php?g=Api&m=ToCode&a=get_code',
        postData = {
          token: app.globalData.token,
          tct_id: '1',
          tit: '',
          con: '',
          page: 'pages/index/index'
        };
      app.postLogin(postUrl, postData, that.setCode);
    }

  },
  randomNum: function (minNum, maxNum) {
    switch (arguments.length) {
      case 1:
        return parseInt(Math.random() * minNum + 1, 10);
        break;
      case 2:
        return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
        break;
      default:
        return 0;
        break;
    }
  },
  randomWishes: function (){
    var self = this;
    const randomWish = self.randomNum(0, app.globalData.wishes.length - 1);
    const baseSource = app.globalData.imgBaseUrl
    var index = '';
    if (randomWish == 0) {
      index = "0";
    } else if (randomWish == 1) {
      index = "1";
    } else {
      index = randomWish.toString();
    };
    self.setData({
      wish_index: randomWish,
      wishes_images: [
        baseSource + index + '-1.png',
        baseSource + index + '-2.png',
        baseSource + index + '-3.png',
        baseSource + index + '-4.png'
      ],
    })
  },
  
  formSubmit:function(e){
    var that = this;
    wx.showLoading({
      title: '',
    })
    wx.showLoading({
      title: '',
      mask: true
    })
    var data = {
      token: that.data.token,
      content: app.globalData.wishes[that.data.wish_index],
      remark: that.data.demoKoulin,
      form_id: e.detail.formId,
    }
    wx.request({
      url: app.setConfig.url + '/index.php/Api/taocaitou/create',
      data: data,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
        if (res.data.code == 20000){
          var tct_id = res.data.data.tct_id
          wx.hideLoading()
          if (app.globalData.tip_message === '') { } else {
            wx.showModal({
              title: '领取成功！',
              content: app.globalData.tip_message,
              showCancel: false
            })
          }
        }else{
          wx.showToast({
            title: res.data.msg,
            icon: 'loading',
            mask: true,
            duration: 1500
          })
        }
      }
    })
  },
  changeoneBtClick:function(){
    var that = this
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'linear',
    })
    that.animation = animation;
    animation.rotateY(90).step()
    that.setData({
      animationData: animation.export()
    })

    setTimeout(function () {
      var nanimation = wx.createAnimation({
        duration: 500,
        timingFunction: 'linear',
      })
      nanimation.rotateY(0).step()
      that.randomWishes()
      that.setData({
        animationData: nanimation.export(),
      })
    }.bind(that), 500)
  },
  loop: function () {
    var that = this
    var info = app.globalData.userInfo,
        tok = app.globalData.token;
    if (info && !that.data.hasUserInfo){
      that.setData({
        userInfo: info,
        hasUserInfo: true
      })
    }
    if (!tok) {
      setTimeout(function () { that.loop(); }, 10)
    } else {
      wx.setClipboardData({
        data: app.globalData.news_key,
        success: function (res) {
        }
      })
      wx.hideLoading()
      var randomNum = that.randomNum(0, app.globalData.demoKoulin.length - 1);
      var demoremark = app.globalData.demoKoulin[randomNum];
      that.setData({
        token: tok,
        demoKoulin: demoremark,
        show_view: true,
        wishImg: app.globalData.imgBaseUrl + '1.png',
      })
      that.randomWishes();
      
    }
  },
  setCode: function (res) {
    var that = this;
    if (res.data.code === 20000) {
      var datas = res.data;
      that.setData({
        xcxewm: app.setConfig.url + '/' + datas.data,
      })
      that.imgBuild();
    }
  },
  //转发
  onShareAppMessage: function (res) {
    var that = this;
    var title = that.data.demoKoulin;
    return {
      title: title,
      path: 'pages/index/index',
      imageUrl: app.globalData.imgBaseUrl + 'sharefriend.png'
    }
  },
  //生成朋友圈分享图
  wxzone: function () {
    var that = this;
    if (that.data.xcxewm === '') {
      //二维码
      var postUrl = app.setConfig.url + '/index.php?g=Api&m=ToCode&a=get_code',
        postData = {
          token: that.data.token,
          tct_id: '1',
          tit: '',
          con: '',
          page: 'pages/index/index'
        };
      app.postLogin(postUrl, postData, that.setCode);
    }else{
      that.imgBuild();
    }
  },
  imgBuild: function () {
    var that = this;
    var fximg = that.data.xcxewm;
    console.log(fximg)
    console.log(1)
    wx.previewImage({
      current: fximg, //当前显示图片的http链接
      urls: [fximg]//需要预览的图片http链接列表
    })
  },
  //获取formid
  formSubmit2: function (e) {
    var postData = {
      form_id: e.detail.formId,
      token: app.globalData.token
    };
    wx.request({
      url: app.setConfig.url + '/index.php?g=Api&m=Taocaitou&a=saveFormid',
      data: postData,
      method: 'POST',
      header: { 'content-type': 'application/x-www-form-urlencoded' },
      success: function (res) {
      }
    })
  }
})
